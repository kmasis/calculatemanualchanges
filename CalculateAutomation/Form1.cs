﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CalculateAutomation
{
    public partial class CalculateAutomation : Form
    {
        //The difference  between both  
        //code bases  is  calculated  using  
        //CLOCversion  1.64 (http://cloc.sourceforge.net).
        //The  total  number  of  modified  lines  is  calculated  as  the  sum  of modified,  
        //added  and removedlines of code. 
        //The command line is the following:cloc-1.64.exe --diff <GreenCodeFolder> <TargetFolder> --include-lang=C# --ignore-whitespace --diff-timeout 60

        //The total lines of code of the Green Code is calculated with cloc as follows:cloc-1.64.exe<GreenCodeFolder>--include-lang=C#

        public CalculateAutomation()
        {
            InitializeComponent();
            this.BringToFront();
            this.StartPosition = FormStartPosition.CenterScreen;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            CalculateManualChanges frm2 = new CalculateManualChanges();
            frm2.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            LinesOfSolution frm = new LinesOfSolution();
            frm.Show();
        }
    }
}
