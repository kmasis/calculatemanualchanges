﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CalculateAutomation
{
    public partial class CalculateManualChanges : Form
    {
        public CalculateManualChanges()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            this.comboBoxLanguages.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {

                if (this.textBoxGreen.Text.Trim().Equals(String.Empty))
                {
                    MessageBox.Show("Green path not valid");
                    return;
                }
                else
            if (this.textBoxTarget.Text.Trim().Equals(String.Empty))
                {
                    MessageBox.Show("Target path not valid");
                    return;
                }
                else
            if (!Directory.Exists(this.textBoxGreen.Text.Trim()))
                {
                    MessageBox.Show("Green directory does not exist");
                    return;
                }
                else
            if (!Directory.Exists(this.textBoxTarget.Text.Trim()))
                {
                    MessageBox.Show("Target directory does not exist");
                    return;
                }

                // WORKING directory (i.e. \bin\Debug)
                string workingDirectory = Environment.CurrentDirectory;
                string projectDirectory = Directory.GetParent(workingDirectory).Parent.FullName;

                if (comboBoxLanguages.SelectedItem.ToString().Equals("ALL"))
                {              
                    System.Diagnostics.Process.Start("CMD.exe", "/K " +
                    projectDirectory + @"\cloc-1.72.exe " + "--diff "
                    + this.textBoxGreen.Text.Trim() + " " + this.textBoxTarget.Text.Trim()
                    + " --ignore-whitespace --diff-timeout 60 --include-lang=C#,CSS,HTML,TypeScript --exclude-dir=bin,obj,node_modules,wwwroot,UpgradeSupport");
                }
                else
                {
                    System.Diagnostics.Process.Start("CMD.exe", "/K " +
                    projectDirectory + @"\cloc-1.72.exe " + "--diff "
                    + this.textBoxGreen.Text.Trim() + " " + this.textBoxTarget.Text.Trim()
                    + " --include-lang="+ comboBoxLanguages.SelectedItem.ToString() + " --ignore-whitespace --diff-timeout 60");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
    }
}
