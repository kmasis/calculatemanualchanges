﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CalculateAutomation
{
    public partial class LinesOfSolution : Form
    {
        public LinesOfSolution()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            this.comboBoxLanguages.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {

                if (this.textBoxPath.Text.Trim().Equals(String.Empty))
                {
                    MessageBox.Show("Path not valid");
                    return;
                }

            if (!Directory.Exists(this.textBoxPath.Text.Trim()))
                {
                    MessageBox.Show("Directory does not exist");
                    return;
                }
                 // WORKING directory (i.e. \bin\Debug)
                string workingDirectory = Environment.CurrentDirectory;
                string projectDirectory = Directory.GetParent(workingDirectory).Parent.FullName;

                if(comboBoxLanguages.SelectedItem.ToString().Equals("ALL"))
                    System.Diagnostics.Process.Start("CMD.exe", "/K " +
                        projectDirectory + @"\cloc-1.72.exe " + this.textBoxPath.Text.Trim() + " --include-lang=C#,CSS,HTML,TypeScript --exclude-dir=bin,obj,node_modules,wwwroot,UpgradeSupport");
                else
                //cloc-1.64.exe <CodeFolder> --include-lang=C#
                System.Diagnostics.Process.Start("CMD.exe", "/K " +
                    projectDirectory + @"\cloc-1.72.exe " + this.textBoxPath.Text.Trim() + " --include-lang=" + comboBoxLanguages.SelectedItem.ToString() +" --exclude-dir=bin,obj,node_modules,wwwroot,UpgradeSupport");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
